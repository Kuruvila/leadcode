package com.logical.programs;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAndTime {

	public static void main(String[] args) {
		Date date = new Date();
	      SimpleDateFormat sformate = new SimpleDateFormat("MMM/d/yyyy");
	      SimpleDateFormat sformate1 = new SimpleDateFormat("hh:mm ss aa");
	      String s = sformate.format(date);
	      String s1 = sformate1.format(date);
	      System.out.println("Current Date " + s);
	      System.out.println("Current Time " + s1);
	}

}
